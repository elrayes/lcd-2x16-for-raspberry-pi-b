This is a modification of the 2x16 character LCD C/C++ library by Asion (https://bitbucket.org/Asion/raspberrypi/src/91a08ad427783db68e3cc653c19f39fedf87d557/lcd/?at=default) for Raspberry Pi B+ using BCM2835 GPIO library, the original library uses WiringPi GPIO library.

This library is dependent on the BCM2835 C/C++ library, you must include it in your project prior building, it can be found here:
http://www.airspayce.com/mikem/bcm2835/index.html