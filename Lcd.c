//Requires bcm2835 GPIO library from: http://www.airspayce.com/mikem/bcm2835/
#include "bcm2835.h"
#include "Lcd.h"
#include <stdio.h>

void setDefaultHd44780(hd44780 * toDefault)
{
	if(!toDefault)
		return;

	toDefault->D4 = RPI_BPLUS_GPIO_J8_33;
	toDefault->D5 = RPI_BPLUS_GPIO_J8_35;
	toDefault->D6 = RPI_BPLUS_GPIO_J8_36;
	toDefault->D7 = RPI_BPLUS_GPIO_J8_37;

	toDefault->registerSelect = RPI_BPLUS_GPIO_J8_38;
	toDefault->enable = RPI_BPLUS_GPIO_J8_40;

	toDefault->colNumber = 2;
	toDefault->rowNumber = 16;

}

void initializeDisplay(hd44780 * header)
{
	if(!header)
		return;

	if (!bcm2835_init())
	{
		//return 1;
		printf("BCM2835 Not initializied\n");
	}

	bcm2835_gpio_fsel(header->D4, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(header->D5, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(header->D6, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(header->D7, BCM2835_GPIO_FSEL_OUTP);
	
	bcm2835_gpio_fsel(header->registerSelect, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(header->enable, BCM2835_GPIO_FSEL_OUTP);
}

static void pulse(hd44780 * header)
{
	bcm2835_delay(1);

	bcm2835_gpio_write(header->enable, LOW);

    bcm2835_delay(1);

	bcm2835_gpio_write(header->enable, HIGH);

	bcm2835_delay(1);

	bcm2835_gpio_write(header->enable, LOW);

	bcm2835_delay(1);

}

void writeBytes(hd44780 * header, int byte, int mode)
{
	if(!header)
		return;

    bcm2835_gpio_write(header->registerSelect, mode);

	bcm2835_delay(1);

	bcm2835_gpio_write(header->D4,LOW);
	bcm2835_gpio_write(header->D5,LOW);
	bcm2835_gpio_write(header->D6,LOW);
	bcm2835_gpio_write(header->D7,LOW);


	if(byte & 0x10)
		bcm2835_gpio_write(header->D4,HIGH);

	if(byte & 0x20)
		bcm2835_gpio_write(header->D5,HIGH);

	if(byte & 0x40)
		bcm2835_gpio_write(header->D6,HIGH);
	if(byte & 0x80)
		bcm2835_gpio_write(header->D7,HIGH);
	
	pulse(header);

	bcm2835_gpio_write(header->D4,LOW);
	bcm2835_gpio_write(header->D5,LOW);
	bcm2835_gpio_write(header->D6,LOW);
	bcm2835_gpio_write(header->D7,LOW);

	if(byte & 0x01)
		bcm2835_gpio_write(header->D4,HIGH);

	if(byte & 0x02)
		bcm2835_gpio_write(header->D5,HIGH);

	if(byte & 0x04)
		bcm2835_gpio_write(header->D6,HIGH);

	if(byte & 0x08)
		bcm2835_gpio_write(header->D7,HIGH);
	
     
    pulse(header);
 
}

void moveCursor(hd44780 * header, cursorMovement movement)
{
	if(!header)
		return;

	if(movement == CURSOR_RIGTH)
		writeBytes(header, 0x14, 0);
	else if(movement == CURSOR_LEFT)
		writeBytes(header, 0x10, 0);
	else 
		writeBytes(header, 0x02, 0);

}

void printString(hd44780 * header, char * string)
{
	if(!header || !string ||!*string)
		return; 

	int positionInLine = 0;

    int i;	
	for(i = 0; string[i] != '\0' ;i++)
	{
		if(positionInLine == header->rowNumber)
		{
		    writeBytes(header, LCD_DDRAMADDRESS | 0xC0, LCD_COMMAND_MODE); //jump to next line
		    positionInLine = 0;
		}

		if(string[i] == '\n' )
		{
			writeBytes(header, LCD_DDRAMADDRESS | 0xC0, LCD_COMMAND_MODE);
			positionInLine = 0;
		}
		else
		{
			writeBytes(header, string[i], LCD_CHARACTER_MODE);
			positionInLine = positionInLine + 1;
		}
	}

}

void scrollDisplay(hd44780 * header, displayScroll scroll)
{
	if(!header)
		return;

	if(scroll == DISPLAY_SCROLLEFT)
	{
		writeBytes(header,LCD_CURSORSHIFT | LCD_SHIFTDISPLAY | LCD_LEFT , LCD_COMMAND_MODE);
	}
	else if(scroll == DISPLAY_SCROLLRIGTH)
	{
		writeBytes(header,LCD_CURSORSHIFT | LCD_SHIFTDISPLAY | LCD_RIGHT , LCD_COMMAND_MODE);
	}
}

void clearDisplay(hd44780 * header)
{
	if(!header)
		return;

	writeBytes(header, 0x01, 0);
}

void printInt32(hd44780 * header, int val)
{
	if(!header)
		return;

	int i;
	for(i = 0; i < 32; i++)
	{
		if(i == header->rowNumber)
		    writeBytes(header, LCD_DDRAMADDRESS | 0xC0, LCD_COMMAND_MODE); //jump to next line

		if((val>>i) & 0x01)
			writeBytes(header, 0x31, 1);
		else
			writeBytes(header, 0x30, 1);

	}
}e l r a y e s  
 